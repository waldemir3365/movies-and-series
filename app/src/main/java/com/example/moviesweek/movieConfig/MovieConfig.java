package com.example.moviesweek.movieConfig;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */
import com.example.moviesweek.interfaces.MovieService;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class MovieConfig {

    private static Retrofit retrofit;

    public MovieConfig(String base) {

        this.retrofit = new Retrofit.Builder()
                        .baseUrl(base)
                        .addConverterFactory(JacksonConverterFactory.create())
                        .build();
    }

    public MovieService getMovieService(){
        return this.retrofit.create(MovieService.class);
    }
}
