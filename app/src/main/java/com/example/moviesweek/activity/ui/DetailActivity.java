package com.example.moviesweek.activity.ui;

/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import com.example.moviesweek.R;
import com.example.moviesweek.constants.Constants;
import com.example.moviesweek.dao.MovieDao;
import com.example.moviesweek.holder.MovieViewHolder;
import com.example.moviesweek.model.*;
import com.example.moviesweek.util.PicassoUtil;
import com.squareup.picasso.Picasso;
public class DetailActivity extends AppCompatActivity {

    private TextView name;
    private ImageView img_fav;
    private ImageView poster;
    private TextView genres;
    private TextView premied;
    private TextView sinopse;
    private Toolbar toolbar;
    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        toolbar = findViewById(R.id.tbl);
        name = findViewById(R.id.name);
        poster = findViewById(R.id.img_poster);
        genres = findViewById(R.id.txt_genres);
        premied = findViewById(R.id.txt_premied);
        sinopse = findViewById(R.id.txt_sinopse);
        img_fav = findViewById(R.id.img_fav);
        init();
    }

    private void init() {
        getDataMovie();
        setValues();
    }

    private void setValues() {
        setFields();
        setValuesToobar();
        img_fav.setOnClickListener(clickInFavorito(movie));
    }

    private View.OnClickListener clickInFavorito(Movie movie) {
        return view->{

                MovieDao dao = new MovieDao(getBaseContext());

                if(movie.getFavorito() == 0){

                    img_fav.setImageResource(R.drawable.ic_favorite_black);
                    movie.setFavorito(1);
                    dao.updateMovie(movie.getId(),movie.getFavorito());

                }else{

                    img_fav.setImageResource(R.drawable.ic_favorite);
                    movie.setFavorito(0);
                    dao.updateMovie(movie.getId(),movie.getFavorito());
                }
        };
    }

    private void setValuesToobar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(backToListMovies());
    }

    private View.OnClickListener backToListMovies() {
        return view->{
                buttonBack();
        };
    }

    private void buttonBack() {
        finish();
    }

    private void setFields() {
        setPoster(movie.getImage().getOriginal());
        verifyImageFavorito(movie.getFavorito(),img_fav);
        name.setText(movie.getName());
        genres.setText(movie.concatItens(movie.getGenres()));
        premied.setText(movie.getPremiered());
        sinopse.setText(validationSummary(movie.getSummary()));
    }
    private String validationSummary(String summary) {

        String nome = summary;

        if(nome.contains("<p>")){

            nome = nome.replace("<p>", "");
        }

        if(nome.contains("</p>")){

            nome = nome.replace("</p>", "");
        }

        if(nome.contains("<b>")){

            nome = nome.replace("<b>", "");
        }

        if(nome.contains("</b>")){

            nome = nome.replace("</b>", "");
        }

        return nome;
    }

    private void setPoster(String original) {
        PicassoUtil.setImage(getBaseContext(),poster,original);
    }

    private void verifyImageFavorito(int favorito, ImageView img_fav) {
        if (favorito == 1) {
            img_fav.setImageResource(R.drawable.ic_favorite_black);
        }
    }

    private void getDataMovie() {
        movie = (Movie) getIntent().getSerializableExtra(Constants.MOVIE);
    }

}
