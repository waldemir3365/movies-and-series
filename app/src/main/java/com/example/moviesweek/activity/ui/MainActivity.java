package com.example.moviesweek.activity.ui;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */
import com.example.moviesweek.dao.MovieDao;
import com.example.moviesweek.movieConfig.MovieConfig;
import com.example.moviesweek.adapter.MovieAdapter;
import com.example.moviesweek.constants.Constants;
import com.example.moviesweek.interfaces.MovieOnclickListener;
import com.example.moviesweek.model.*;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.TextView;
import com.example.moviesweek.R;
import com.example.moviesweek.util.NavigationUtil;
import com.example.moviesweek.util.ToastUtil;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView empty;
    private ViewHolder mViewHolder = new ViewHolder();
    private ListMovie show;
    private MovieDao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewHolder.recycler = findViewById(R.id.recycler_view);
        toolbar = findViewById(R.id.toolbar);
        empty = findViewById(R.id.txt_empty);
        init();
    }

    private void init(){
        setToobar();
        Connection();
    }

    private void setToobar() {
        setSupportActionBar(toolbar);
    }

    private void Connection() {
        ConnectivityManager conexao = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = conexao.getActiveNetworkInfo();
        verifyConnectionToService(info);
    }

    private void verifyConnectionToService(NetworkInfo info) {
        if(info != null && info.isConnected()){
            empty.setVisibility(View.GONE);
            getCall();
        }
        else{
            empty.setText(getString(R.string.notNetwork));
            empty.setVisibility(View.VISIBLE);
            ToastUtil.ToastLong(getBaseContext(), getString(R.string.notNetwork_s));
        }
    }

    private void getCall() {
        Call<List<Movie>> call = new MovieConfig(Constants.URL_BASE).getMovieService().getShows();
        call.enqueue(getServiceCall());
    }

    private Callback<List<Movie>> getServiceCall() {
        return new Callback<List<Movie>>() {
            @Override
            public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
                show = new ListMovie();
                show.setMovie(response.body());
                setDataInList();
                setAdapterList(show.getMovie());
                closeDao();
            }
            @Override
            public void onFailure(Call<List<Movie>> call, Throwable t) {
                ToastUtil.ToastLong(getBaseContext(),getString(R.string.notDataServer));
                Log.e(getString(R.string.errorServer), t.getMessage());
            }
        };
    }

    private void closeDao() {
        dao.close();
    }

    private void setDataInList() {
        setSingleInsert();
        setDataListCurrent(show.getMovie());
    }

    private void setDataListCurrent(List<Movie> movie) {
        SQLiteDatabase db = dao.getReadableDatabase();
        ArrayList<Movie> listBancoMovie = dao.getAllMovies(db);
            for (int i = 0; i < movie.size(); i++) {
                movie.get(i).setFavorito(listBancoMovie.get(i).getFavorito());
            }
    }

    private void setSingleInsert() {
        for(int i = 0; i < show.getMovie().size();i++){
                dao = new MovieDao(getBaseContext());
                SQLiteDatabase db = dao.getReadableDatabase();

                    if(!dao.verifyIdMovie(db,show.getMovie().get(i).getId())){
                        dao.insertMovie(show.getMovie().get(i));
                    }
        }
    }

    private void setAdapterList(List<Movie> movie) {
        mViewHolder.recycler.setAdapter(new MovieAdapter(getBaseContext(),movie,clicklist()));
        setLayoutList();
    }

    private MovieOnclickListener clicklist() {
        return new MovieOnclickListener() {
            @Override
            public void OnclickListener(Movie movie) {
                NavigationUtil.navigate(getBaseContext(), DetailActivity.class, movie);
            }
        };
    }
    private void setLayoutList() {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        mViewHolder.recycler.setLayoutManager(layoutManager);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setDataListCurrent(show.getMovie());
        setAdapterList(show.getMovie());
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_busca);
        setSearchView(menuItem);
        return true;
    }

    private void setSearchView(MenuItem menuItem) {
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint(getString(R.string.text_hint_search));
        searchView.setOnQueryTextListener(getTextSearchView());
    }

    private SearchView.OnQueryTextListener getTextSearchView() {
        return new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                //recovery Text SearchView
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        };
    }

    public class ViewHolder{
        RecyclerView recycler;
    }
}
