package com.example.moviesweek.model;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
@JsonIgnoreProperties({"score","_links","updated","externals","webChannel","network","weight","rating",
                        "schedule","officialSite","runtime","status","language","type","url"})
public class Movie implements Serializable {

    private int id;
    private String name;
    private List<String> genres;
    private String premiered;
    private Image image;
    private String summary;
    private int favorito = 0;


    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getPremiered() {
        return premiered;
    }

    public void setPremiered(String premiered) {
        this.premiered = premiered;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String concatItens(List<String> genres) {
        String nome = "";

            for (String genero: genres) {
                    nome = nome.concat(genero + ", ");
            }
        return nome;
    }
}
