package com.example.moviesweek.model;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties({"medium"})
public class Image implements Serializable {

    private String original;

    public String getOriginal() {
        return original;
    }
    public void setOriginal(String original) {
        this.original = original;
    }
}
