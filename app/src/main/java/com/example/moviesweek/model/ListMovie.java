package com.example.moviesweek.model;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */

import java.util.List;
public class ListMovie {

    private List<Movie> movie;
    public List<Movie> getMovie() {
        return movie;
    }
    public void setMovie(List<Movie> movie) {
        this.movie = movie;
    }
}
