package com.example.moviesweek.interfaces;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */
import com.example.moviesweek.model.Movie;

public interface MovieOnclickListener {
    void OnclickListener(Movie movie);
}
