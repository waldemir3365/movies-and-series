package com.example.moviesweek.interfaces;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */

import com.example.moviesweek.model.Movie;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
public interface MovieService {

    @GET("search/shows")
    Call<Movie> getFilme(@Query("q") String param);

    @GET("shows?page=0")
    Call<List<Movie>> getShows();
}

