package com.example.moviesweek.holder;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */
import com.example.moviesweek.dao.MovieDao;
import com.example.moviesweek.model.*;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.moviesweek.R;
import com.example.moviesweek.interfaces.MovieOnclickListener;
import com.example.moviesweek.util.PicassoUtil;

public class MovieViewHolder extends RecyclerView.ViewHolder {

    private TextView nome;
    private ImageView favorite;
    private ImageView poster;
    private TextView genrer;
    private Context context;
    private CardView cardView;

    public MovieViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        nome = itemView.findViewById(R.id.text_nome);
        favorite = itemView.findViewById(R.id.img_favorite);
        cardView = itemView.findViewById(R.id.card_view);
        poster = itemView.findViewById(R.id.img_poster);
        genrer = itemView.findViewById(R.id.text_genrer);
    }

    public void setFields(Movie movie, MovieOnclickListener listener) {
        setFavorite(movie);
        nome.setText(movie.getName());
        setPoster(movie.getImage().getOriginal());
        genrer.setText(movie.concatItens(movie.getGenres()));
        cardView.setOnClickListener(clickItemList(movie, listener));
        favorite.setOnClickListener(clickListFavorite(movie));
    }

    private void setFavorite(Movie movie) {
        if (movie.getFavorito() == 1) {
            favorite.setImageResource(R.drawable.ic_favorite_black);
        }
    }

    private View.OnClickListener clickListFavorite(Movie movie) {
        return view->{
                MovieDao dao = new MovieDao(context);

                if(movie.getFavorito() == 0){
                    favorite.setImageResource(R.drawable.ic_favorite_black);
                    movie.setFavorito(1);
                    dao.updateMovie(movie.getId(),movie.getFavorito());
                }else{

                    favorite.setImageResource(R.drawable.ic_favorite);
                    movie.setFavorito(0);
                    dao.updateMovie(movie.getId(),movie.getFavorito());
                }
        };
    }

    private void setPoster(String image) {
        PicassoUtil.setImage(context,poster,image);
    }

    private View.OnClickListener clickItemList(Movie movie, MovieOnclickListener listener) {
        return view->{
            listener.OnclickListener(movie);
        };
    }
}
