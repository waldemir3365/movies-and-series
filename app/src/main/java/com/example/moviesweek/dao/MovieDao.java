package com.example.moviesweek.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.moviesweek.model.*;
import java.util.ArrayList;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */
public class MovieDao extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MovieDB";
    private static final String TABELA_MOVIE = "Movie";
    private static final String ID = "id";
    private static final String FAVORITO = "favorito";

    private static final String[] COLUNAS = {ID, FAVORITO};

    public MovieDao(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE Movie(" +
                "id INTEGER PRIMARY KEY," +
                "favorito INTEGER)";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Movie");
        this.onCreate(db);
    }

    public void insertMovie(Movie m) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = getDataContentValues(m.getId(), m.getFavorito());
        db.insert(TABELA_MOVIE, null, values);
        db.close();
    }

    private ContentValues getDataContentValues(int id, int favorito) {
        ContentValues values = new ContentValues();
        values.put(ID, id);
        values.put(FAVORITO, favorito);
        return values;
    }

    public void updateMovie(int id, int favorito){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = getDataContentValues(id, favorito);
        String[] params = {String.valueOf(id)};
        db.update("Movie", values, "id = ?", params);
    }

    public Boolean verifyIdMovie(SQLiteDatabase db, int id){
        Cursor c = db.query(TABELA_MOVIE, null, "id = ?", new String[]{String.valueOf(id)}, null, null, null);
        if (c.moveToFirst()) {
            instanceMovie(c);
            return true;
        } else return false;
    }

    private void instanceMovie(Cursor c) {
        Movie movie = new Movie();
        movie.setId(c.getInt(0));
        movie.setFavorito(c.getInt(1));
    }

    public ArrayList<Movie> getAllMovies(SQLiteDatabase db) {
        ArrayList<Movie> listMovie = new ArrayList<Movie>();
        Cursor c = db.query(TABELA_MOVIE,
                null, null, null, null, null, null);
        while (c.moveToNext()) {
            Movie m = new Movie();
            m.setId(c.getInt(0));
            m.setFavorito(c.getInt(1));
            listMovie.add(m);
        }
        return listMovie;
    }
}
