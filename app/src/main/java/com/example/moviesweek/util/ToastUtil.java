package com.example.moviesweek.util;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */
import android.content.Context;
import android.widget.Toast;

public class ToastUtil {

    public static void ToastLong(Context context, String msg){
        Toast.makeText(context,msg,Toast.LENGTH_LONG);
    }

    public static void Toast(Context context, String msg){
       Toast.makeText(context,msg,Toast.LENGTH_SHORT);
    }
}
