package com.example.moviesweek.util;

import android.content.Context;
import android.widget.ImageView;

import com.example.moviesweek.R;
import com.squareup.picasso.Picasso;

public class PicassoUtil {

    public static void setImage(Context context, ImageView image, String url){
        Picasso.with(context)
                .load(url)
                .error(R.drawable.ic_launcher_background)
                .into(image);
    }
}
