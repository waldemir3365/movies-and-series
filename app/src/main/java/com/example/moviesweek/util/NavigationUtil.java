package com.example.moviesweek.util;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */
import android.content.Context;
import android.content.Intent;
import com.example.moviesweek.constants.Constants;
import com.example.moviesweek.model.*;
public class NavigationUtil {

    private static Intent i;

    public static void navigate(Context context, Class contextoDestino, Movie movie){
        i = new Intent(context, contextoDestino);
        i.putExtra(Constants.MOVIE,movie);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
