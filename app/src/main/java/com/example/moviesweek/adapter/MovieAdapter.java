package com.example.moviesweek.adapter;
/*
    Create by developer Waldemir Gomes in data 06/04/2019.
 */
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.moviesweek.R;
import com.example.moviesweek.holder.MovieViewHolder;
import com.example.moviesweek.interfaces.MovieOnclickListener;
import com.example.moviesweek.model.Movie;
import java.util.List;
public class MovieAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<Movie> listMovie;
    private MovieOnclickListener listener;

    public MovieAdapter(Context context, List<Movie> listmovie, MovieOnclickListener listener) {
        this.context = context;
        this.listMovie = listmovie;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_movie_list, viewGroup, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        MovieViewHolder holder = (MovieViewHolder) viewHolder;
        Movie movie = listMovie.get(position);
        holder.setFields(movie,listener);
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }
}